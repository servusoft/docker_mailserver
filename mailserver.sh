#!/bin/bash
clear
set -e
echo "Welcome to the Mailserver from http://crank.zone"
echo "Setup Database"
echo "required ENV"
echo "DB_HOST,DB_NAME,DB_USER,DB_PASS"

# defining mail name
echo "localhost" > /etc/mailname
# adding IP of a host to /etc/hosts
export HOST_IP=$(/sbin/ip route|awk '/default/ { print $3 }')
echo "$HOST_IP dockerhost" >> /etc/hosts

# check ENV
if [[ -z $DB_HOST ]];
then
	exit 10
fi
if [[ -z $DB_NAME ]];
then
	exit 11
fi
if [[ -z $DB_USER ]];
then
	exit 12
fi
if [[ -z $DB_PASS ]];
then
	exit 13
fi
echo "Database Setup"
mysqlResult="$(mysql -u$DB_USER -p$DB_PASS -h$DB_HOST -se "CREATE DATABASE IF NOT EXISTS $DB_NAME;" )"
echo "RESULT: $mysqlResult"
mysqlResult="$(mysql -u$DB_USER -p$DB_PASS -h$DB_HOST $DB_NAME -se "CREATE TABLE IF NOT EXISTS users (id INT UNSIGNED AUTO_INCREMENT NOT NULL, username VARCHAR(128) NOT NULL, domain VARCHAR(128) NOT NULL, password VARCHAR(128) NOT NULL, UNIQUE (id), PRIMARY KEY (username, domain) );" )"
echo "RESULT: $mysqlResult"
mysqlResult="$(mysql -u$DB_USER -p$DB_PASS -h$DB_HOST $DB_NAME -se "CREATE TABLE IF NOT EXISTS domains (id INT UNSIGNED AUTO_INCREMENT NOT NULL, domain VARCHAR(128) NOT NULL, UNIQUE (id), PRIMARY KEY (domain));" )"
echo "RESULT: $mysqlResult"
mysqlResult="$(mysql -u$DB_USER -p$DB_PASS -h$DB_HOST $DB_NAME -se "CREATE TABLE IF NOT EXISTS aliases (id INT UNSIGNED AUTO_INCREMENT NOT NULL, source VARCHAR(128) NOT NULL, destination VARCHAR(128) NOT NULL, UNIQUE (id), PRIMARY KEY (source, destination) );" )"
echo "RESULT: $mysqlResult"
mysqlResult="$(mysql -u$DB_USER -p$DB_PASS -h$DB_HOST -se "GRANT ALL ON vmail.* TO '$DB_NAME'@'%' IDENTIFIED BY '$DB_PASS';" )"
echo "RESULT: $mysqlResult"

postconf -e myhostname=$HOST_NAME
postconf -F '*/*/chroot =n'
postconf -e smtpd_banner="$myhostname ESMTP $mail_name (Ubuntu)"
postconf -e append_dot_mydomain="no"
postconf -e readme_directory="no"
postconf -e mynetworks="127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128"
postconf -e mydestination=""
postconf -e mailbox_size_limit="51200000"
postconf -e message_size_limit="51200000"
postconf -e recipient_delimiter=""
postconf -e inet_interfaces="all"
postconf -e inet_protocols="all"
postconf -e myorigin=$HOST_NAME
postconf -e smtpd_tls_cert_file="/etc/ssl/certs/ssl-cert-snakeoil.pem"
postconf -e smtpd_tls_key_file="/etc/ssl/private/ssl-cert-snakeoil.key"
postconf -e smtpd_use_tls="yes"
postconf -e smtpd_tls_mandatory_protocols="!SSLv2, !SSLv3"
postconf -e smtpd_tls_session_cache_database="btree:${data_directory}/smtpd_scache"
postconf -e smtp_tls_session_cache_database="btree:${data_directory}/smtp_scache"
postconf -e smtpd_sasl_type="dovecot"
postconf -e smtpd_sasl_path="private/auth"
postconf -e smtpd_sasl_auth_enable="yes"
postconf -e virtual_transport="lmtp:unix:private/dovecot-lmtp"
postconf -e smtpd_recipient_restrictions="permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination"
postconf -e virtual_alias_maps="mysql:/etc/postfix/virtual/mysql-aliases.cf"
postconf -e virtual_mailbox_maps="mysql:/etc/postfix/virtual/mysql-maps.cf"
postconf -e virtual_mailbox_domains="mysql:/etc/postfix/virtual/mysql-domains.cf"
postconf -e local_recipient_maps="$virtual_mailbox_maps"

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/dovecot/conf.d/10-auth.conf

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/dovecot/conf.d/10-auth.conf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/dovecot/conf.d/10-auth.conf

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/dovecot/conf.d/10-mail.conf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/dovecot/conf.d/10-mail.conf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/dovecot/conf.d/10-mail.conf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/dovecot/conf.d/10-mail.conf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/dovecot/conf.d/10-mail.conf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/dovecot/conf.d/10-mail.conf

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/dovecot/conf.d/10-master.conf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/dovecot/conf.d/10-master.conf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/dovecot/conf.d/10-master.conf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/dovecot/conf.d/10-master.conf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/dovecot/conf.d/10-master.conf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/dovecot/conf.d/10-master.conf

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/dovecot/conf.d/10-ssl.conf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/dovecot/conf.d/10-ssl.conf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/dovecot/conf.d/10-ssl.conf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/dovecot/conf.d/10-ssl.conf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/dovecot/conf.d/10-ssl.conf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/dovecot/conf.d/10-ssl.conf

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/dovecot/conf.d/15-lda.conf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/dovecot/conf.d/15-lda.conf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/dovecot/conf.d/15-lda.conf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/dovecot/conf.d/15-lda.conf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/dovecot/conf.d/15-lda.conf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/dovecot/conf.d/15-lda.conf

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/dovecot/dovecot.conf 
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/dovecot/dovecot.conf 
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/dovecot/dovecot.conf 
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/dovecot/dovecot.conf 
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/dovecot/dovecot.conf 
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/dovecot/dovecot.conf 

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/dovecot/dovecot-sql.conf.ext

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/postfix/master.conf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/postfix/master.conf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/postfix/master.conf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/postfix/master.conf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/postfix/master.conf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/postfix/master.conf


sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/postfix/virtual/mysql-aliases.cf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/postfix/virtual/mysql-aliases.cf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/postfix/virtual/mysql-aliases.cf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/postfix/virtual/mysql-aliases.cf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/postfix/virtual/mysql-aliases.cf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/postfix/virtual/mysql-aliases.cf

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/postfix/virtual/mysql-maps.cf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/postfix/virtual/mysql-maps.cf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/postfix/virtual/mysql-maps.cf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/postfix/virtual/mysql-maps.cf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/postfix/virtual/mysql-maps.cf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/postfix/virtual/mysql-maps.cf

sed -i "s/{{DB_HOST}}/$DB_HOST/g" /etc/postfix/virtual/mysql-domains.cf
sed -i "s/{{DB_NAME}}/$DB_NAME/g" /etc/postfix/virtual/mysql-domains.cf
sed -i "s/{{DB_USER}}/$DB_NAME/g" /etc/postfix/virtual/mysql-domains.cf
sed -i "s/{{DB_PASS}}/$DB_PASS/g" /etc/postfix/virtual/mysql-domains.cf
sed -i "s/{{HOST_NAME}}/$HOST_NAME/g" /etc/postfix/virtual/mysql-domains.cf
sed -i "s/{{HOST_MAIL}}/$HOST_MAIL/g" /etc/postfix/virtual/mysql-domains.cf

chown -R root:root /etc/postfix/
chmod 555 -R /etc/postfix/

# start logger
rsyslogd 
# start services
service postfix reload
service postfix restart
dovecot -F 