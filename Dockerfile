FROM ubuntu:14.04
MAINTAINER Manuel 'Onko' Müller <mueller.m.h@gmail.com>

ENV HOST_NAME myhostname.de
ENV HOST_MAIL mm@crankdev.de
ENV DB_HOST db
ENV DB_USER root
ENV DB_PASS password
ENV DB_NAME	vmail

RUN apt-get update && apt-get upgrade -y
RUN apt-get install dovecot-common dovecot-imapd dovecot-mysql dovecot-lmtpd mysql-client -y
RUN apt-get install postfix postfix-mysql -y

# add Host Filesystem
RUN mkdir /var/vmail
RUN useradd vmail
RUN chown -R vmail:vmail /var/vmail
RUN chmod -R 770 /var/vmail/
RUN mkdir /etc/postfix/virtual
RUN sudo chmod 660 /etc/postfix/virtual/

COPY 10-auth.conf /etc/dovecot/conf.d/10-auth.conf
COPY 10-auth.conf /etc/dovecot/conf.d/10-auth.conf
COPY 10-mail.conf /etc/dovecot/conf.d/10-mail.conf
COPY 10-master.conf /etc/dovecot/conf.d/10-master.conf
COPY 10-ssl.conf /etc/dovecot/conf.d/10-ssl.conf
COPY 15-lda.conf /etc/dovecot/conf.d/15-lda.conf
COPY dovecot.conf /etc/dovecot/dovecot.conf 
COPY dovecot-sql.conf.ext /etc/dovecot/dovecot-sql.conf.ext
COPY postfix_master.conf /etc/postfix/master.conf

COPY mysql-aliases.cf /etc/postfix/virtual/mysql-aliases.cf
COPY mysql-maps.cf /etc/postfix/virtual/mysql-maps.cf
COPY mysql-domains.cf /etc/postfix/virtual/mysql-domains.cf

# SMTP ports
EXPOSE 25
EXPOSE 587  
# POP and IMAP ports  
EXPOSE 110
EXPOSE 143
EXPOSE 995
EXPOSE 993

RUN chmod -R 777 /var/log/
COPY mailserver.sh /mailserver.sh
RUN chmod +x /mailserver.sh

CMD /mailserver.sh